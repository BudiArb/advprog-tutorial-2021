package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CodexTranslatorAdapter implements Transformation {

    private CodexTranslator codexTranslator;

    public CodexTranslatorAdapter() {
        codexTranslator = new CodexTranslator();
    }

    @Override
    public Spell encode(Spell spell) {
        return codexTranslator.translate(spell, RunicCodex.getInstance());
    }

    @Override
    public Spell decode(Spell spell) {
        return codexTranslator.translate(spell, AlphaCodex.getInstance());
    }
}
