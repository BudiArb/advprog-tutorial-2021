package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :) FINISH
public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean aimShotMode;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        aimShotMode = false;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(aimShotMode);
    }

    @Override
    public String chargedAttack() {
        if (aimShotMode) {
            aimShotMode = false;
            return "Leaving Aim Shot Mode";
        } else {
            aimShotMode = true;
            return "Entering Aim Shot Mode";
        }
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me FINISH
        return bow.getHolderName();
    }
}
