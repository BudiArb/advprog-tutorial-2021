package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;
import java.util.Collections;

public class ChainSpell implements Spell {
    private ArrayList<Spell> spells, undoSpells;

    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
        this.undoSpells = new ArrayList<>(spells);
        Collections.reverse(this.undoSpells);
    }

    @Override
    public void cast() {
        for (Spell spell : spells) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for (Spell spell : undoSpells) {
            spell.undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
