package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me FINISH
        this.guild = guild;
    }

    //ToDo: Complete Me FINISH
    public void update() {
        Quest quest = this.guild.getQuest();
        String questType = this.guild.getQuestType();
        if (!questType.equals("Rumble")) {
            this.getQuests().add(quest);
        }
    }
}
