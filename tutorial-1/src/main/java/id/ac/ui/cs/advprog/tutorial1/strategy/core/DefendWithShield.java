package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    //ToDo: Complete me FINISH
    public String getType() {
        return "DefendWithShield";
    }

    public String defend() {
        return "clank";
    }
}
