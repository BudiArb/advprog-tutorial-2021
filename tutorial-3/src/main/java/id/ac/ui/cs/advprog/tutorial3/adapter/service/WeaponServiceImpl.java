package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// TODO: Complete me. Modify this class as you see fit~ FINISH
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private WeaponRepository weaponRepository;

    // TODO: implement me FINISH
    @Override
    public List<Weapon> findAll() {
        for (Bow bow : bowRepository.findAll()) {
            weaponRepository.save(new BowAdapter(bow));
        }

        for (Spellbook spellbook : spellbookRepository.findAll()) {
            weaponRepository.save(new SpellbookAdapter(spellbook));
        }

        return weaponRepository.findAll();
    }

    // TODO: implement me FINISH
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);

        String attackLog;
        String attackTypeStr;

        if (attackType == 0) {
            attackLog = weapon.normalAttack();
            attackTypeStr = "normal attack";
        } else {
            attackLog = weapon.chargedAttack();
            attackTypeStr = "charged attack";
        }

        logRepository.addLog(
                weapon.getHolderName() + " attacked with " + weaponName + " (" + attackTypeStr + "): " +attackLog);
        
        weaponRepository.save(weapon);
    }

    // TODO: implement me FINISH
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
