package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    //ToDo: Complete me FINISH
    public String getType() {
        return "DefendWithArmor";
    }

    public String defend() {
        return "plank";
    }
}
