package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        //ToDo: Complete Me FINISH
        this.guild = guild;
    }

    //ToDo: Complete Me FINISH
    public void update() {
        Quest quest = this.guild.getQuest();
        String questType = this.guild.getQuestType();
        if (!questType.equals("Escort")) {
            this.getQuests().add(quest);
        }
    }
}
