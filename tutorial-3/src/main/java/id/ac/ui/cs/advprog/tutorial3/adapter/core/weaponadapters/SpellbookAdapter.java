package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :) FINISH
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean chargedAttackAvailabe;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        chargedAttackAvailabe = true;
    }

    @Override
    public String normalAttack() {
        chargedAttackAvailabe = true;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (chargedAttackAvailabe) {
            chargedAttackAvailabe = false;
            return spellbook.largeSpell();
        } else {
            return getHolderName() + " is too exhausted from the last spell. Cast other spell!";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
