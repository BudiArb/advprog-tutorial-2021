package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class BlitzTransformation implements Transformation {
    @Override
    public Spell encode(Spell spell){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        String firstChar = text.substring(0, 1);
        String remainingText = text.substring(1);
        return new Spell(remainingText+firstChar, codex);
    }

    @Override
    public Spell decode(Spell spell){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int textSize = text.length();
        String lastChar = text.substring(textSize-1);
        String remainingText = text.substring(0, textSize-1);
        return new Spell(lastChar+remainingText, codex);
    }
}
